from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from webdriver_manager.chrome import ChromeDriverManager
import time

base_url = "https://indeed.nl"

def prompt_function():
    function = input("Voer een functie in: ")

    return function

def promp_location():
    location = input("Voer een locatie in (stad, postcode, etc): ")

    return location

def format_url(function, location):
    url_template = base_url + "/jobs?q={}&l={}"
    url = url_template.format(function, location)

    return url

def prompt_pages_amount():
    amount = input("Hoeveel pagina's wil je scrapen? ")

    return int(amount)

function = prompt_function()
location = promp_location()
number_of_pages = prompt_pages_amount()

url = format_url(function, location)

driver = webdriver.Chrome(ChromeDriverManager().install())

def go_to_url(url):
    driver.get(url)

    time.sleep(5)

    html = driver.page_source

    return html

def get_page_source(html):
    return BeautifulSoup(html, "html.parser")

html = go_to_url(url)

soup = get_page_source(html)

found_ads = soup.find_all(class_="resultContent")

def get_ads(found_ads):
    arr = []

    for ad in found_ads:
        title = ad.find(class_="jobTitle")
        company = ad.find(class_="companyName")
        salary = ad.find(class_="salary-snippet-container")

        string_output = title.text + " | " + company.text

        if salary:
            string_output += " | " + salary.text
        else:
            string_output += " | salaris onbekend"

        arr.append(string_output)

    return arr

all_ads = get_ads(found_ads)

next_page_element = soup.find("a", attrs={"aria-label":"Next Page"})

if next_page_element:
    url_to_go = base_url + next_page_element["href"]

for x in range(1, number_of_pages):
    if next_page_element:
        html = go_to_url(url_to_go)

        soup = get_page_source(html)

        try:
            driver.find_element(By.XPATH, "//div[@class='google-Only-Modal-Upper-Row']//button[@aria-label='Close']").click()
        except NoSuchElementException:
            print("Google inloggen pop up niet gevonden")

        try:
            driver.find_element(By.XPATH, "//button[@class='icl-CloseButton' and @aria-label='sluiten']").click()
        except NoSuchElementException:
            print("Indeed pop up niet gevonden")

        ads_inside_page = soup.find(class_="resultContent")
        all_ads += get_ads(ads_inside_page)

        next_page_element = soup.find("a", attrs={"aria-label":"Next Page"})

        if next_page_element:
            url_to_go = base_url + next_page_element["href"]

print("\nTotale aantal gescrapte vacatures: " + str(len(all_ads)) + "\n")
print(*all_ads, sep="\n"*2)

driver.close()